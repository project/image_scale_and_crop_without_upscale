<?php

/**
 * @file
 * Post Updates hooks for the Image Scale and Crop Without Upscale module.
 */

/**
 * Update image styles to use store width/height as integers in config.
 */
function image_scale_and_crop_without_upscale_post_update_fix_data_types(&$sandbox): void {
  // Manually update the configuration for image styles using our effect to
  // modify the width and height to be integers instead of strings.
  // A much easier way is to simply load and save affected image styles, as
  // Drupal will automatically cast the values. But this will invoke the
  // postSave function of ImageStyle, which will cause a flush of the entire
  // image style. Looking to avoid that as some sites may have thousands of
  // existing images which are all perfectly fine.
  $configFactory = \Drupal::configFactory();
  foreach ($configFactory->listAll('image.style.') as $configId) {
    $config = $configFactory->getEditable($configId);
    foreach ($config->get('effects') as $uuid => $effect) {
      if ($effect['id'] === 'image_scale_and_crop_without_upscale' && (is_string($effect['data']['width']) || is_string($effect['data']['height']))) {
        $config->set("effects.{$uuid}.data.width", (int) $effect['data']['width']);
        $config->set("effects.{$uuid}.data.height", (int) $effect['data']['height']);
        $config->save(TRUE);
      }
    }
  }
}
