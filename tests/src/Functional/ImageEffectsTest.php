<?php

namespace Drupal\Tests\image_scale_and_crop_without_upscale\Functional;

use Drupal\Core\Image\ImageInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\Traits\Core\Image\ToolkitTestTrait;
use Drupal\image\ImageEffectManager;

/**
 * Tests that the image effects pass parameters to the toolkit correctly.
 *
 * @group image
 */
class ImageEffectsTest extends BrowserTestBase {

  use ToolkitTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'image_scale_and_crop_without_upscale',
    'image_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The image effect manager.
   */
  protected ImageEffectManager $imageEffectPluginManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->imageEffectPluginManager = $this->container->get('plugin.manager.image.effect');
  }

  /**
   * Test the image_resize_effect() function.
   *
   * Not using a data provider for test data since it's so much slower.
   */
  public function testScaleAndCropWithoutUpscaleEffect() {
    // The source image width and height is always 200x200.
    $testData = [
      // Source width < target width, source height < target height, same A/R.
      // Target should be equal to source.
      [400, 400, 0, 0, 200, 200],
      // Source width < target width, source height < target height, diff A/R.
      // Target should be downscaled, maintaining A/R.
      [400, 300, 0, 25, 200, 150],
      // Source width < target width, source height = target height.
      // Target should be downscaled, maintaining A/R.
      [300, 200, 0, 33.5, 200, 133],
      // Source width < target width, source height > target height.
      // Target should be downscaled, maintaining A/R.
      [300, 100, 0, 66.5, 200, 67],
      // Source width > target width, source height > target height, same A/R.
      // Target should be downscaled, maintaining A/R.
      [100, 100, 0, 0, 100, 100],
      // Source width > target width, source height < target height, diff A/R.
      // Source should be downscaled & cropped, maintaining A/R.
      [100, 300, 67, 0, 66, 200],
      // Source width > target width, source height = target height.
      // Source should be downscaled & cropped, maintaining A/R.
      [100, 200, 50, 0, 100, 200],
      // Source width > target width, source height > target height.
      // Source should be downscaled & cropped, maintaining A/R.
      [100, 50, 0, 25, 100, 50],
    ];
    foreach ($testData as $data) {
      $targetWidth = $data[0];
      $targetHeight = $data[1];
      $cropX = $data[2];
      $cropY = $data[3];
      $resultWidth = $data[4];
      $resultHeight = $data[5];

      $this->assertImageEffect(['scale_and_crop'], 'image_scale_and_crop_without_upscale', [
        'width' => $targetWidth,
        'height' => $targetHeight,
      ]);

      // Check the parameters.
      $calls = $this->imageTestGetAllCalls();
      $this->assertEquals($calls['scale_and_crop'][0][0], $cropX, 'X was computed and passed correctly for data set ' . serialize($data));
      $this->assertEquals($calls['scale_and_crop'][0][1], $cropY, 'Y was computed and passed correctly for data set ' . serialize($data));
      $this->assertEquals($calls['scale_and_crop'][0][2], $resultWidth, 'Width was computed and passed correctly for data set ' . serialize($data));
      $this->assertEquals($calls['scale_and_crop'][0][3], $resultHeight, 'Height was computed and passed correctly for data set ' . serialize($data));
    }
  }

  /**
   * Sets up an image with the custom toolkit.
   *
   * Override from ToolkitTestTrait so we can provide our own source image.
   *
   * @return \Drupal\Core\Image\ImageInterface
   *   The image object.
   */
  protected function getImage(): ImageInterface {
    $image_factory = \Drupal::service('image.factory');
    $file_uri = \Drupal::service('extension.list.module')->getPath('image_scale_and_crop_without_upscale') . '/tests/images/200x200.png';
    $image = $image_factory->get($file_uri, 'test');
    $this->assertTrue($image->isValid());
    return $image;
  }

}
