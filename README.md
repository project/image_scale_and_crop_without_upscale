CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides a scale and crop image effect that will not upscale
small source images to fit the target width/height, which is what main
scale and crop effect provided by Drupal core does.

If a source image is smaller than the target dimensions, it will still be
cropped to the correct aspect ratio as defined by the target dimensions.

This lets you define a image effect that will always crop an image
to a specific aspect ratio and downscale it to a maximum resolution.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/image_scale_and_crop_without_upscale

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/image_scale_and_crop_without_upscale


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/node/1897420 for further information.

 * Navigate to Administration > Extend and enable the module


CONFIGURATION
-------------

 * The module has no modifiable settings. There is no configuration. When
   enabled, the module will scale the images maintaining the aspect ratio
   preventing pixelation. To undo the changes, disable the module and
   clear caches.

MAINTAINERS
-----------

Current maintainers:
 * Brian Osborne (bkosborne) - https://www.drupal.org/u/bkosborne
